import numpy as np
from mip import *


def blotto_x_response(q, y, a, c):
        
    assert(c <= 0.5)    

    n = len(a)
    k = len(q)    
    
    M1_l = 1/c - 1
    M1_u = 1/c + 1
    M2_l = 1/c + 1
    M2_u = 1/c - 1    
    
    model = Model()

    Vn = range(n)
    Vk = range(k)

    x  = [ model.add_var(var_type=CONTINUOUS, name="x", lb=0.) for i in Vn ]
    s1 = [[ model.add_var(var_type=CONTINUOUS, name="s1", lb=0.) for i in Vn ] for j in Vk] #s1[j,i]
    s2 = [[ model.add_var(var_type=CONTINUOUS, name="s2", lb=0.) for i in Vn ] for j in Vk]
    z1 = [[ model.add_var(var_type=BINARY, name="z1") for i in Vn ] for j in Vk]
    z2 = [[ model.add_var(var_type=BINARY, name="z2") for i in Vn ] for j in Vk] 

    model += xsum( x[i] for i in Vn ) == 1
    
    for i in Vn:
        for j in Vk:
            model += s1[j][i] >= 1/c*(x[i] - y[j,i] + c)    
            model += s1[j][i] <= 1/c*(x[i] - y[j,i] + c) + M1_l*(1-z1[j][i])
            model += s1[j][i] <= M1_u*z1[j][i]

    for i in Vn:
        for j in Vk:
            model += s2[j][i] >= 1/c*(x[i] - y[j,i] - c)    
            model += s2[j][i] <= 1/c*(x[i] - y[j,i] - c) + M2_l*(1-z2[j][i])
            model += s2[j][i] <= M2_u*z2[j][i]
        
    model.objective = maximize(xsum( q[j]*xsum( a[i]*(s1[j][i]-s2[j][i]-1) for i in Vn ) for j in Vk))
    
    status = model.optimize()
    
    return model.objective_value



def solve_x(y, a, c):

    assert(c <= 0.5)    

    n  = len(a)    
    Vn = range(n)
    
    M1_l = 1/c - 1
    M1_u = 1/c + 1
    M2_l = 1/c + 1
    M2_u = 1/c - 1    
    
    model = Model()

    x  = [ model.add_var(var_type=CONTINUOUS, name="x", lb=0.) for i in Vn ]
    s1 = [ model.add_var(var_type=CONTINUOUS, name="s1", lb=0.) for i in Vn ]
    s2 = [ model.add_var(var_type=CONTINUOUS, name="s2", lb=0.) for i in Vn ]
    z1 = [ model.add_var(var_type=BINARY, name="z1") for i in Vn ]
    z2 = [ model.add_var(var_type=BINARY, name="z2") for i in Vn ]

    model += xsum( x[i] for i in Vn ) == 1
    
    for i in Vn:
        model += s1[i] >= 1/c*(x[i] - y[i] + c)    
        model += s1[i] <= 1/c*(x[i] - y[i] + c) + M1_l*(1-z1[i])
        model += s1[i] <= M1_u*z1[i]

    for i in Vn:
        model += s2[i] >= 1/c*(x[i] - y[i] - c)    
        model += s2[i] <= 1/c*(x[i] - y[i] - c) + M2_l*(1-z2[i])
        model += s2[i] <= M2_u*z2[i]
        
    model.objective = maximize(xsum( a[i]*(s1[i]-s2[i]-1) for i in Vn ))
    
    status = model.optimize()
    
    return model.objective_value



def solve_x_linear(y, a, c):

    
    n = len(a)
    
    assert(n == 3)
    
    obj_max = -np.Inf    
    
    for k in range(3):
        for l in range(3):
            for m in range(3):
                model = Model()
                x = [ model.add_var(var_type=CONTINUOUS, name="x", lb=0.) for i in range(n) ]
                d = [ model.add_var(var_type=CONTINUOUS, name="d", lb=-1.) for i in range(n) ]
                
                if k == 0:
                    model += d[0] == -1
                    model += x[0] - y[0] <= -c
                elif k == 1:
                    model += d[0] == 1/c*(x[0] - y[0])
                    model += x[0] - y[0] >= -c
                    model += x[0] - y[0] <= c
                elif k == 2:
                    model += d[0] == 1
                    model += x[0] - y[0] >= c
                    
                if l == 0:
                    model += d[1] == -1
                    model += x[1] - y[1] <= -c
                elif l == 1:
                    model += d[1] == 1/c*(x[1] - y[1])
                    model += x[1] - y[1] >= -c
                    model += x[1] - y[1] <= c
                elif l == 2:
                    model += d[1] == 1
                    model += x[1] - y[1] >= c
    
                if m == 0:
                    model += d[2] == -1
                    model += x[2] - y[2] <= -c
                elif m == 1:
                    model += d[2] == 1/c*(x[2] - y[2])
                    model += x[2] - y[2] >= -c
                    model += x[2] - y[2] <= c
                elif m == 2:
                    model += d[2] == 1
                    model += x[2] - y[2] >= c
                    
                model += xsum( x[i] for i in range(n) ) == 1
                model.objective = maximize(xsum( a[i]*d[i] for i in range(n) ))
                
                status = model.optimize()
    
                if (status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE) and model.objective_value >= obj_max:
                    obj_max = model.objective_value
    
    return obj_max
        

def blotto_y_response(p, x, a, c):
        
    assert(c <= 0.5)    

    n = len(a)
    k = len(p)    

    M1_l = 1/c - 1
    M1_u = 1/c + 1
    M2_l = 1/c + 1
    M2_u = 1/c - 1    
    
    model = Model()

    Vn = range(n)
    Vk = range(k)

    y  = [ model.add_var(var_type=CONTINUOUS, name="y", lb=0.) for i in Vn ]
    s1 = [[ model.add_var(var_type=CONTINUOUS, name="s1", lb=0.) for i in Vn ] for j in Vk] #s1[j,i]
    s2 = [[ model.add_var(var_type=CONTINUOUS, name="s2", lb=0.) for i in Vn ] for j in Vk]
    z1 = [[ model.add_var(var_type=BINARY, name="z1") for i in Vn ] for j in Vk]
    z2 = [[ model.add_var(var_type=BINARY, name="z2") for i in Vn ] for j in Vk] 

    model += xsum( y[i] for i in Vn ) == 1
    
    for i in Vn:
        for j in Vk:
            model += s1[j][i] >= 1/c*(x[j][i] - y[i] + c)    
            model += s1[j][i] <= 1/c*(x[j][i] - y[i] + c) + M1_l*(1-z1[j][i])
            model += s1[j][i] <= M1_u*z1[j][i]

    for i in Vn:
        for j in Vk:
            model += s2[j][i] >= 1/c*(x[j][i] - y[i] - c)    
            model += s2[j][i] <= 1/c*(x[j][i] - y[i] - c) + M2_l*(1-z2[j][i])
            model += s2[j][i] <= M2_u*z2[j][i]
        
    model.objective = minimize(xsum( p[j]*xsum( a[i]*(s1[j][i]-s2[j][i]-1) for i in Vn ) for j in Vk))
    
    status = model.optimize()
    
    return model.objective_value



def solve_y(x, a, c):

    assert(c <= 0.5)    

    n  = len(a)    
    Vn = range(n)
    
    M1_l = 1/c - 1
    M1_u = 1/c + 1
    M2_l = 1/c + 1
    M2_u = 1/c - 1    
    
    model = Model()

    y  = [ model.add_var(var_type=CONTINUOUS, name="y", lb=0.) for i in Vn ]
    s1 = [ model.add_var(var_type=CONTINUOUS, name="s1", lb=0.) for i in Vn ]
    s2 = [ model.add_var(var_type=CONTINUOUS, name="s2", lb=0.) for i in Vn ]
    z1 = [ model.add_var(var_type=BINARY, name="z1") for i in Vn ]
    z2 = [ model.add_var(var_type=BINARY, name="z2") for i in Vn ]

    model += xsum( y[i] for i in Vn ) == 1
    
    for i in Vn:
        model += s1[i] >= 1/c*(x[i] - y[i] + c)    
        model += s1[i] <= 1/c*(x[i] - y[i] + c) + M1_l*(1-z1[i])
        model += s1[i] <= M1_u*z1[i]

    for i in Vn:
        model += s2[i] >= 1/c*(x[i] - y[i] - c)    
        model += s2[i] <= 1/c*(x[i] - y[i] - c) + M2_l*(1-z2[i])
        model += s2[i] <= M2_u*z2[i]
        
    model.objective = minimize(xsum( a[i]*(s1[i]-s2[i]-1) for i in Vn ))
    
    status = model.optimize()
    
    return model.objective_value



def solve_y_linear(x, a, c):

    
    n = len(a)
    
    assert(n == 3)
    
    obj_min = np.Inf    
    
    for k in range(3):
        for l in range(3):
            for m in range(3):
                model = Model()
                y = [ model.add_var(var_type=CONTINUOUS, name="y", lb=0.) for i in range(n) ]
                d = [ model.add_var(var_type=CONTINUOUS, name="d", lb=-1.) for i in range(n) ]
                
                if k == 0:
                    model += d[0] == -1
                    model += x[0] - y[0] <= -c
                elif k == 1:
                    model += d[0] == 1/c*(x[0] - y[0])
                    model += x[0] - y[0] >= -c
                    model += x[0] - y[0] <= c
                elif k == 2:
                    model += d[0] == 1
                    model += x[0] - y[0] >= c
                    
                if l == 0:
                    model += d[1] == -1
                    model += x[1] - y[1] <= -c
                elif l == 1:
                    model += d[1] == 1/c*(x[1] - y[1])
                    model += x[1] - y[1] >= -c
                    model += x[1] - y[1] <= c
                elif l == 2:
                    model += d[1] == 1
                    model += x[1] - y[1] >= c
    
                if m == 0:
                    model += d[2] == -1
                    model += x[2] - y[2] <= -c
                elif m == 1:
                    model += d[2] == 1/c*(x[2] - y[2])
                    model += x[2] - y[2] >= -c
                    model += x[2] - y[2] <= c
                elif m == 2:
                    model += d[2] == 1
                    model += x[2] - y[2] >= c
                    
                model += xsum( y[i] for i in range(n) ) == 1
                model.objective = minimize(xsum( a[i]*d[i] for i in range(n) ))
                
                status = model.optimize()

                if (status == OptimizationStatus.OPTIMAL or status == OptimizationStatus.FEASIBLE) and model.objective_value <= obj_min:
                    obj_min = model.objective_value
    
    return obj_min


    
n = 3
k = 5
c = 0.01        


a = np.random.rand(n)

y = np.random.rand(n)    
y /= sum(y)
y_mod1 = np.zeros((1,n))
y_mod2 = np.zeros((k,n))
for j in range(1):
    y_mod1[j,:] = y
for j in range(k):
    y_mod2[j,:] = y

q = np.random.rand(2)
q /= sum(q)

    
obj1 = solve_x_linear(y, a, c)
obj2 = solve_x(y, a, c)
obj3 = blotto_x_response([1.], y_mod1, a, c)
obj4 = blotto_x_response(q, y_mod2, a, c)
 
print((obj1 - obj2) / abs(obj1))
print((obj1 - obj3) / abs(obj1))
print((obj1 - obj4) / abs(obj1))


obj1 = solve_y_linear(y, a, c)
obj2 = solve_y(y, a, c)
obj3 = blotto_y_response([1.], y_mod1, a, c)
obj4 = blotto_y_response(q, y_mod2, a, c)
 
print((obj1 - obj2) / abs(obj1))
print((obj1 - obj3) / abs(obj1))
print((obj1 - obj4) / abs(obj1))






    
