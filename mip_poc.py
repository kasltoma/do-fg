#!/usr/bin/python
# -*- coding: utf-8 -*-

#developed for Python 3.8
#author - Tomas Kasl, FEL CVUT

import numpy as np
from mip import *

n = 3
m = 4

c = [0.17488126, 0.14001708, 0.83823246]
d = [0.08353532, 0.38994883, 0.19445643, 0.02628565]

A = [[0.92279558, 0.28247788, 0.28346748],
       [0.68801942, 0.59581569, 0.4453662 ],
       [0.21758134, 0.59910156, 0.30246135]]

B = [[0.18755954, 0.02525863, 0.18467912, 0.44704156],
       [0.95179993, 0.54124298, 0.52477476, 0.42162525],
       [0.06139825, 0.49997553, 0.44551948, 0.04592954]]

b = [0.89140111, 0.36743268, 0.52962352]

model = Model()
x = [ model.add_var(var_type=CONTINUOUS, name="x") for i in range(n) ]
z = [ model.add_var(var_type=BINARY, name="z") for i in range(m) ]

for row in range(n):
    A_ = A[row]
    B_ = B[row]
    model += xsum( A_[i] * x[i] for i in range(n) ) + xsum(B_[i] * z[i] for i in range(m) ) >= b[row], "row"+str(row)

print("conditions")
print(model.constr_by_name("row0"))
print(model.constr_by_name("row1"))
print(model.constr_by_name("row1"))

model.objective = minimize( xsum(c[i]*x[i] for i in range(n)) + xsum(d[i]*z[i] for i in range(m)) )

status = model.optimize()


print("status = ", status)
print("value = ", model.objective_value)
print("odhad pri ABORT = ", model.objective_bound)
print("vars")
for v in model.vars:
       print('{} : {}'.format(v.name, v.x))