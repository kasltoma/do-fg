#!/usr/bin/python
# -*- coding: utf-8 -*-

#developed for Python 3.8
#author - Tomas Kasl, FEL CVUT

import numpy as np
import scipy.optimize as sp
import matplotlib.pyplot as plt
import timeit

from util import *
from games import *
from plots import *
from validation import *

#############################

#possible values are "random", "previous" and "fminbound"
# - random chooses random point as x0 for gradient optimization
# - previous chooses outcome of previous DO iteration as x0
# - NP function for bounded optimization is used (NON-GRADIENT) - works only for dim(X)=dim(Y)=1

#optimizationStrategy = "random"
#optimizationStrategy = "previous"
#optimizationStrategy = "fminbound"
optimizationStrategy = "discrete-best"
#optimizationStrategy = "discrete-worst"
#optimizationStrategy = "discrete-random"

roundStrategies = False
strategyRoundingDecimals = 3

validate = False
breakOnEquilibrium = False

def initValues(game, random=True):
    if random:
        x = game.X.getRandomPoint()
        y = game.Y.getRandomPoint()
    else:
        x_bounds = game.X.getCube()
        x = [ x_t[0] for x_t in x_bounds ]
        y_bounds = game.X.getCube()
        y = [ y_t[0] for y_t in y_bounds ]
    return x,y

##############################
            
def solve(game, method, maxitr, printout=True):
    #Modify numpy array printing
    np.set_printoptions(edgeitems=30, linewidth=1000, formatter=dict(float=lambda x: "%.9g" % x))
    #random init strategies
    x, y = initValues(game, True)
    #used in the loop for convergence check
    prev_x, prev_y = x, y
    xs, ys = np.array([x]), np.array([y])
    x_responses, y_responses = np.array([x]), np.array([y])
    if printout:
        print("init x's = ", xs)
        print("init y's = ", ys)
    if method == "DO":
        #init starting game matrix
        matrix = np.array( game.u(xs, ys) )   
        if(printout):
            print("x's: ", xs)
            print("y's: ", ys)
            print("init matrix is ", matrix)
    xs_distribution = np.array([1])
    ys_distribution = np.array([1])
    #containers for values of the game
    upper_bounds = [-mixed_utility_function_x(x, ys, ys_distribution, game.u)[0] ]
    lower_bounds = [ mixed_utility_function_y(y, xs, xs_distribution, game.u)[0] ]  
##################### LOOP #######################
    itr = 0
    while itr < maxitr:
        itr += 1

        #find best pure response
        x = optimal_response('a', ys, ys_distribution, optimizationStrategy, game.u, game.X, prev_x)
        y = optimal_response('b', xs, xs_distribution, optimizationStrategy, game.u, game.Y, prev_y)
  
        if validate and len(x)==1:
            assert(validate_x_response(x, ys, ys_distribution, game))
            assert(validate_y_response(y, xs, xs_distribution, game))

        upper_bounds.append(-mixed_utility_function_x(x, ys, ys_distribution, game.u)[0] )
        lower_bounds.append( mixed_utility_function_y(y, xs, xs_distribution, game.u)[0] )   
        
        if roundStrategies:
            y = np.round(y, strategyRoundingDecimals)
            x = np.round(x, strategyRoundingDecimals)
        if(printout):
            print("x response = ", x)
            print("y response = ", y)
        x_responses = np.insert(x_responses, 0, values=x, axis=0)
        y_responses = np.insert(y_responses, 0, values=y, axis=0)
        prex_x = x
        prev_y = y
        
        if method == "DO":
            if not element_in_array(x, xs):
                xs, matrix = extend_matrix_x(game, matrix, x, xs, ys)
            if not element_in_array(y, ys):
                ys, matrix = extend_matrix_y(game, matrix, y, ys, xs)
            if(printout):
                print("iterative matrix")
                print(matrix)
            xs_distribution = optimal_mixed_strategy(matrix, player='a')
            ys_distribution = optimal_mixed_strategy(matrix, player='b')

        elif method == "FP":
            coef = 1/(itr+1)
            xs_distribution = xs_distribution * ( (itr)*coef)
            ys_distribution = ys_distribution * ( (itr)*coef)
            if element_in_array(x, xs):
                xs_distribution[np.where(xs==x)[0]] += coef
            else:
                xs = np.insert(xs, 0, values=x, axis=0)
                xs_distribution = np.insert(xs_distribution, 0, values=coef, axis=0)
            if element_in_array(y, ys):
                ys_distribution[np.where(ys==y)[0]] += coef
            else:
                ys = np.insert(ys, 0, values=y, axis=0)
                ys_distribution = np.insert(ys_distribution, 0, values=coef, axis=0)

        else:
            print("Only DO (double oracle) and FP (fictional play) methods are supported")
            exit()

        if validate:
            assert(validate_distribution(xs_distribution))
            assert(validate_distribution(ys_distribution))
                
        if(printout):
            print("------------------------------------------------------------------------")
            print("Mixed x strategy: ", xs_distribution)
            print("Mixed y strategy: ", ys_distribution)
        
        #check convergence
        if itr > 1 and np.abs(upper_bounds[-1] - lower_bounds[-1]) < game.epsilon and breakOnEquilibrium:
            print("converge found with values x:", x, "y: ", y, "upper and lower bounds: ", upper_bounds[-1], lower_bounds[-1], "iteration: ", itr+1)
            break
##################### OUTPUT #######################
    if(printout):
        print("Mixed x strategy: ", xs_distribution)
        print("xs", xs)
        print("Mixed y strategy: ", ys_distribution)
        print("ys", ys)

    return np.flip(xs.T[0]), np.flip(x_responses.T[0]), np.flip(xs_distribution), np.flip(ys.T[0]), np.flip(y_responses.T[0]), np.flip(ys_distribution), lower_bounds, upper_bounds, itr+1

######################################


def DO_vs_FP(games, iters, runs):
    for game in games:    
        run = 0
        while run < runs:
            print("game"+game.label+", run ", run)
            try:
                xs_DO, _, x_dist_DO, ys_DO, _, y_dist_DO, lower_bounds_DO, upper_bounds_DO, _ = solve(game, "DO", iters, printout=False)
                _, x_responses_FP, _, _, y_responses_FP, _, lower_bounds_FP, upper_bounds_FP, _ = solve(game, "FP", iters, printout=False)
            except:
                continue
            plot_DO_vs_FP_value(game, lower_bounds_DO, upper_bounds_DO, lower_bounds_FP, upper_bounds_FP, run)
            plot_DO_vs_FP_strategy(game, x_responses_FP, y_responses_FP, xs_DO, ys_DO, x_dist_DO, y_dist_DO, run)
            run += 1


def benchmark(games, method="DO", iters=20, runs=10, text=""):
    for game in games:    
        run = 0
        while run < runs:
            print("game"+game.label+", run ", run)
            try:
                xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds, itr = solve(game, method, iters, printout=False)
            except:
                continue
            #plot_game(game, xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds, run=run, strat=optimizationStrategy, save=True, label=game.label+"_"+method)
            plot_output(game, xs, ys, x_responses, y_responses, x_dist, y_dist, lower_bounds, upper_bounds, method, run, text)
            run += 1

game = game6
games = [game6]
method = "FP"
iterations = 15

#benchmark(games, method="FP", iters=iterations, runs=1, text="100iters_")
DO_vs_FP(games, iterations, 1)
#plot_util_function(game2.u, -1,1,-1,1)
#plot_util_function(game6.u, -2.25, 2.5, -2.5, 1.75)

#xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds, itr = solve(game, method, iterations, printout=True)

#plot_output(game, xs, ys, x_responses, y_responses, x_dist, y_dist, lower_bounds, upper_bounds, method, 0, "PKS")

#plot_game(game, xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds)
input()
