#!/usr/bin/python
# -*- coding: utf-8 -*-

#developed for Python 3.8
#author - Tomas Kasl, FEL CVUT

import numpy as np
import scipy.optimize as sp
import matplotlib.pyplot as plt
import timeit

from util import *
from games import *
from plots import *
from validation import *
from blotto import *

#############################

c = 0.0625

roundStrategies = False
strategyRoundingDecimals = 6

validate = False

##############################

def initValues(game, random=True):
    if random:
        x = game.X.getRandomPoint()
        y = game.Y.getRandomPoint()
    else:
        x_bounds = game.X.getCube()
        x = [ x_t[0] for x_t in x_bounds ]
        y_bounds = game.X.getCube()
        y = [ y_t[0] for y_t in y_bounds ]
    return x,y
            
def solve(game, method, maxitr, printout=True):
    #Modify numpy array printing
    np.set_printoptions(edgeitems=30, linewidth=1000, formatter=dict(float=lambda x: "%.9g" % x))
    #random init strategies
    x, y = initValues(game, True)
    #used in the loop for convergence check
    prev_x, prev_y = x, y
    xs, ys = np.array([x]), np.array([y])
    x_responses, y_responses = np.array([x]), np.array([y])
    if printout:
        print("init x's = ", xs)
        print("init y's = ", ys)
    if method == "DO":
        #init starting game matrix
        matrix = np.array( [blotto_util(np.array([x]), np.array([y]), game.a, game.c)] )   
        if(printout):
            print("x's: ", xs)
            print("y's: ", ys)
            print("init matrix is ", matrix)
    xs_distribution = np.array([1])
    ys_distribution = np.array([1])
    
    #containers for values of the game
    upper_bounds = [blotto_x_util(x, ys, ys_distribution, game.a, game.c) ]
    lower_bounds = [blotto_y_util(y, xs, xs_distribution, game.a, game.c) ]   
##################### LOOP #######################
    itr = 0
    while itr < maxitr:
        itr += 1
        #find best pure response
        x = blotto_x_response(ys_distribution, ys, game.a, game.c)
        y = blotto_y_response(xs_distribution, xs, game.a, game.c)
  
        upper_bounds.append( blotto_x_util(x, ys, ys_distribution, game.a, game.c) )
        lower_bounds.append( blotto_y_util(y, xs, xs_distribution, game.a, game.c) )   
        
        if roundStrategies:
            y = np.round(y, strategyRoundingDecimals)
            x = np.round(x, strategyRoundingDecimals)
        if(printout):
            print("x response = ", x)
            print("y response = ", y)

        x_responses = np.insert(x_responses, 0, values=x, axis=0)
        y_responses = np.insert(y_responses, 0, values=y, axis=0)
        prex_x = x
        prev_y = y
        
        if method == "DO":
            if not element_in_array(x, xs):
                xs, matrix = extend_blotto_matrix_x(x, xs, ys, game)
            if not element_in_array(y, ys):
                ys, matrix = extend_blotto_matrix_y(y, xs, ys, game)
            if(printout):
                print("iterative matrix")
                print(matrix)
            xs_distribution = optimal_mixed_strategy(matrix, player='a')
            ys_distribution = optimal_mixed_strategy(matrix, player='b')
        elif method == "FP":
            coef = 1/(itr+1)
            xs_distribution = xs_distribution * ( (itr)*coef)
            ys_distribution = ys_distribution * ( (itr)*coef)
            if element_in_array(x, xs):
                xs_distribution[np.where(xs==x)[0]] += coef
            else:
                xs = np.insert(xs, 0, values=x, axis=0)
                xs_distribution = np.insert(xs_distribution, 0, values=coef, axis=0)
            if element_in_array(y, ys):
                ys_distribution[np.where(ys==y)[0]] += coef
            else:
                ys = np.insert(ys, 0, values=y, axis=0)
                ys_distribution = np.insert(ys_distribution, 0, values=coef, axis=0)
            
        else:
            print("Only DO (double oracle) and FP (fictional play) methods are supported")
            exit()
        
        if validate:
            assert(validate_distribution(xs_distribution))
            assert(validate_distribution(ys_distribution))
                
        if(printout):
            print("------------------------------------------------------------------------")
            print("Mixed x strategy: ", xs_distribution)
            print("Mixed y strategy: ", ys_distribution)
        
        #check convergence
        if itr > 1 and np.abs(upper_bounds[-1] - lower_bounds[-1]) < game.epsilon:
            print("converge found with values x:", xs[0], "y: ",  ys[0], "upper and lower bounds: ", upper_bounds[-1], lower_bounds[-1], "iteration: ", itr+1)
            #break
        
##################### OUTPUT #######################
    if(printout):
        print("Mixed x strategy: ", xs_distribution)
        print("xs", xs)
        print("Mixed y strategy: ", ys_distribution)
        print("ys", ys)
        print("lower bounds", lower_bounds)
        print("upper bounds", upper_bounds)

    return np.flip(xs.T[0]), np.flip(x_responses.T[0]), np.flip(xs_distribution), np.flip(ys.T[0]), np.flip(y_responses.T[0]), np.flip(ys_distribution), lower_bounds, upper_bounds, itr+1

######################################

game = blotto_3
xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds, itr = solve(game, "DO", 30, printout=True)
print("xs")
print(x_responses)

#benchmark([game], method="DO", iters=10)
#benchmark(games, method="DO", iters=20)

plot_game(game, xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds)
plot_game(game, xs, x_responses, x_dist, ys, y_responses, y_dist, lower_bounds, upper_bounds, save=True, label="Blotto")
input()